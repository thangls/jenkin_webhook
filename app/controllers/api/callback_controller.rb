module Api
	class CallbackController < Api::BaseController
		def unfuddle
			#tracking changeset
			object = Hash.from_xml(request.body.read)
  		changeset = Changeset.create(change_set_params(ActionController::Parameters.new(object)))
  		
  		#handle callback
  		build_config = BuildConfig.where(branch: changeset.branch).take
  		res = request_build(build_config) if build_config
  		p res
  		
			render json: {success: true}, status: :ok
		end

		private
		def change_set_params(params)
			params.require(:changeset).permit(:author_date, :author_email, :author_id, :author_name, 
				:committer_date, :committer_email, :committer_id, :committer_name, :created_at, 
				:message, :repository_id, :revision, :subdomain, :branch)
		end

		def request_build(build_config)
			uri = URI(build_config.url_callback)
			res = Net::HTTP.get_response(uri)
			res.body
		end
	end
end