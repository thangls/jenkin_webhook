# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170216035403) do

  create_table "build_configs", force: :cascade do |t|
    t.string   "branch"
    t.string   "url_callback"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "changesets", force: :cascade do |t|
    t.datetime "author_date"
    t.string   "author_email"
    t.integer  "author_id"
    t.string   "author_name"
    t.datetime "committer_date"
    t.string   "committer_email"
    t.integer  "committer_id"
    t.string   "committer_name"
    t.datetime "created_at",      null: false
    t.string   "message"
    t.integer  "repository_id"
    t.string   "revision"
    t.string   "subdomain"
    t.string   "branch"
    t.datetime "updated_at",      null: false
  end

end
