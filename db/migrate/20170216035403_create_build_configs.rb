class CreateBuildConfigs < ActiveRecord::Migration[5.0]
  def change
    create_table :build_configs do |t|
    	t.string :branch
    	t.string :url_callback
      t.timestamps
    end
  end
end
