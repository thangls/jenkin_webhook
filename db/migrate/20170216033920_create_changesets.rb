class CreateChangesets < ActiveRecord::Migration[5.0]
  def change
    create_table :changesets do |t|
    	t.datetime :author_date
    	t.string :author_email
    	t.integer :author_id
    	t.string :author_name
    	t.datetime :committer_date
    	t.string :committer_email
    	t.integer :committer_id
    	t.string :committer_name
    	t.datetime :created_at
    	t.string :message
    	t.integer :repository_id
    	t.string :revision
    	t.string :subdomain
    	t.string :branch
      t.timestamps
    end
  end
end
